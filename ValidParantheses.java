import java.util.Stack;

class Solution {
    public boolean isValid(String s) {
        // Create a stack to store opening brackets
        Stack<Character> stack = new Stack<>();
        
        // Iterate through the string
        for (char c : s.toCharArray()) {
            // If c is an opening bracket, push it onto the stack
            if (c == '(' || c == '{' || c == '[') {
                stack.push(c);
            } else {
                // If c is a closing bracket
                if (stack.isEmpty()) {
                    return false; // No matching opening bracket
                }
                
                // Pop the top element from the stack
                char top = stack.pop();
                
                // Check if it matches the corresponding opening bracket
                if ((c == ')' && top != '(') || 
                    (c == '}' && top != '{') || 
                    (c == ']' && top != '[')) {
                    return false;
                }
            }
        }
        
        // Check if there are any remaining opening brackets in the stack
        return stack.isEmpty();
    }
}
