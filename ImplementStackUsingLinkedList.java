//{ Driver Code Starts
import java.util.*;

class StackNode {
    int data;
    StackNode next;
    StackNode(int a) {
        data = a;
        next = null;
    }
}

class GfG {

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t > 0) {
            MyStack obj = new MyStack();
            int Q = sc.nextInt();
            while (Q > 0) {
                int QueryType = 0;
                QueryType = sc.nextInt();
                if (QueryType == 1) {
                    int a = sc.nextInt();
                    obj.push(a);
                } else if (QueryType == 2) {
                    System.out.print(obj.pop() + " ");
                }
                Q--;
            }
            System.out.println("");
            t--;
        }
    }
}

// } Driver Code Ends


class MyStack 
{
    StackNode top;
    
    //Function to push an integer into the stack.
    void push(int a) 
    {
        // Create a new node with the given data
        StackNode newNode = new StackNode(a);
        
        // Make the new node the top of the stack
        newNode.next = top;
        top = newNode;
    }
    
    //Function to remove an item from top of the stack.
    int pop() 
    {
        // If the stack is empty, return -1
        if (top == null) {
            return -1;
        }
        
        // Get the data from the top node
        int data = top.data;
        
        // Move the top reference to the next node
        top = top.next;
        
        // Return the popped data
        return data;
    }
}
